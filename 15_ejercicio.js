//(15) Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentra el número mayor. 

var vector=[];

for (i=0; i<10; i++){
    num=parseInt(prompt(`Numero ${i}: `));
    vector[i]=num
}

max = Math.max(...vector);
pos = vector.indexOf(max);

console.log(`El valor maximo es ${max} y se encuentra en la posicion ${pos} del vector`);
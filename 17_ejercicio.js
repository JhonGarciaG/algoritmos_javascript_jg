//(17) Leer 10 números enteros, almacenados en un vector y determinar cuántas veces está repetido el mayor

var vector=[];
var cont = 0;

for (i=0; i<10; i++){
    num=parseInt(prompt(`Numero ${i}: `));
    vector[i]=num
}

let max = Math.max(...vector);

for(i=0; i<10; i++){
    if(vector[i] == max){
        cont += 1;
    }
}

if(cont > 0){
    console.log(`El numero mayor se repite ${cont}  veces`);
}else{
    console.log(`El numero mayor no se repite`);
}
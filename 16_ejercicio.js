//(16) Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentran los números terminados en O

var vector=[];
var pos=[];
var cont_pos = 0 ;

for (i=0; i<10; i++){
    num=parseInt(prompt(`Numero ${i}: `));
    vector[i]=num

    if(num % 10 == 0){
        cont_pos +=1
        pos[cont_pos]=i;
    }
}

if (cont_pos > 0){
    console.log(`Los numeros terminados en 0 se encuentran en las siguientes posiciones: ${pos}`);
}else{
    console.log(`No hay numeros terminados en 0 dentro del vector`);
}

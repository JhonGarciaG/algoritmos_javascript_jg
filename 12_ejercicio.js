// (12) Leer un número entero y determinar cuantas veces tiene el digito 1.
num = parseInt(prompt('Digite un numero entero'));
aux_num = num ;
cont = 0;

while(aux_num>0){        
        dig = aux_num % 10;

        if (dig == 1){
            cont += 1
        }
        aux_num = parseInt(aux_num /10);
}

console.log(`>> El numero : ${num} tiene ${cont} veces el numero 1`);
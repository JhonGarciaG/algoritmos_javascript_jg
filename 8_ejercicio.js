// (8) Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. 

num = parseInt(prompt('Numero:'));

while(num>=10000){
    num=parseInt(prompt('Su numero no cumple la condicion, digite un numero de maximo 4 digitos validos'));
}



dig2 = parseInt((num % 1000)/100);
dig3 = parseInt((num % 100 )/ 10);

if (dig2 == dig3){
    console.log(`${num}: el segundo digito ${dig2}, es igual al penultimo ${dig3}`);
}else{
    console.log(`${num}: el segundo digito ${dig2}, no es igual al penultimo ${dig3}`);
}
//(11) Leer un número entero y determinar a cuánto es igual la suma de sus digitos.

num = parseInt(prompt('Digite un numero entero'))

suma=0
num_aux = num

while(num>0){        
        suma += num % 10;
        num = parseInt(num /10);
}

console.log(`>> La suma de los digitos del numero: ${num_aux} es ${suma}`);
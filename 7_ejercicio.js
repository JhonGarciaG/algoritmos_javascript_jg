// (7) Leer un número entero de dos digitos y determinar si los dos digitos son iguales.

num = parseInt(prompt('Numero:'));

while(num>=100){
    num=parseInt(prompt('Su numero no cumple la condicion, digite un numero de 2 digitos validos));
}

dig1 = num % 10;
dig2 = parseInt(num / 10);

if(dig1 == dig2){
    console.log('Los digitos son iguales');
}else{
    console.log('Los digitos son diferentes');
}
// (6) Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.

num = parseInt(prompt('Numero:'));

while(num>=100){
    num=parseInt(prompt('Su numero no cumple la condicion, digite un numero de 2 digitos validos));
}

dig1 = num % 10;
dig2 = parseInt(num / 10);

if(dig1 % dig2 == 0){
    console.log(`${num}: El digito ${dig1} es multiplo de ${dig2}`);

}else if (dig2 % dig1 == 0){
    console.log(`${num}: El digito ${dig2} es multiplo de ${dig1}`);

}else{
    console.log(`${num}: Ningun digito es multiplo del otro`)
}

// (4) Leer un número entero de dos digitos menor que 20 y determinar si es primo.

let num=parseInt( prompt('Digite un numero de 2 digitos menor de 20'));

while(num>=20 || num<=0){
    num=parseInt(prompt('Su numero no cumple la condicion, digite un numero de 2 digitos validom, menor que 20'));
    console.log(num)
}

if(num==2 || num==3 || num==5 || num==7 || num==11 || num==13 || num==17 || num==19){
    console.log(`El numero ${num} es un numero primo menor que 20`)
}else{
    console.log(`El numero ${num} no es un numero primo`)
}
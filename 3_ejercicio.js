import { Console } from 'node:console';
import prompt from 'prompt';
//(3) Leer un número enterO de dos digitos y determinar si ambos digitos son pares.

let num=parseInt( prompt('Digite un numero de 2 digitos'));

while(num>= 100 || num<=0){
    num=parseInt(prompt('Su numero no cumple la condicion, digite un numero de 2 digitos valido'));
    console.log(num)
}

let dig1 = parseInt(num % 10);
let dig2 = parseInt(num / 10);

if ((dig1 % 2 == 0) && (dig2 % 2 == 1)){
    console.log(`Solo el digito: ${dig1} es par`)
   
}else if((dig1 % 2 == 1) && (dig2 % 2 == 0)){
    console.log(`Solo el digito :${dig2} es par`)

}else if((dig1 % 2 == 0) && (dig2 % 2 == 0)){
    console.log(`Los dos digito: ${dig1} y ${dig2} son pares`)

}else if((dig1 % 2 == 1) && (dig2 % 2 == 1)){
    console.log(`Los dos digito: ${dig1} y ${dig2} son impares`)
}